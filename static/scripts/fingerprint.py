from math import e
import librosa
import uuid
import os
import librosa.display
from scipy.signal import find_peaks

class Fingerprint:
    def __init__(self, audiofile, lower, upper) -> None:
        self.y, self.sr = librosa.load(audiofile)
        self.lowerBound = lower
        self.upperBound = upper
        self.lowerSample = round(self.sr * self.lowerBound)
        self.upperSample = round(self.sr * self.upperBound)
        self.newY = self.y[self.lowerSample: self.upperSample]

    def fingerprint(self):
        try:
            peaksLocations, promimence = find_peaks(self.newY, height = 0 )
            peaks = self.newY[peaksLocations]
            file = open(os.path.join(os.path.dirname(__file__), "fingerprints", str(uuid.uuid4()) + '.txt'), 'w')
            for z in range(len(peaks)):
                file.write(str(peaks[z]))
                file.write(',')
            file.close()
            return True
        except Exception as e:
            print(e)
            return False