# Import all necessary libraries
import os
from datetime import date
import matplotlib.pyplot as plt
import librosa
import librosa.display
import warnings
warnings.filterwarnings("ignore")

class Spectrogram:
    """
    A class used to represent a spectrogram.

    ...

    Attributes
    ----------
    filepath : str
        A string containing the path of the .wav file used to create the spectrogram
    
    Methods
    -------
    generateSpectrogram()
        Generates the spectrogram from the filepath provided in the constructor,
        saves the spectrogram in the images folder and returns a String containing
        the path of the saved spectogram
    """
    def __init__(self, filepath: str) -> None:
        """
        Parameters
        ----------
        filepath: str
            A string containing the path of the .wav file used to create the spectrogram
        """
        self.filepath = filepath

    def generateSpectrogram(self):
        """
        Generates the spectrogram from the filepath provided in the constructor,
        saves the spectrogram in the images folder and returns a String containing
        the path of the saved spectogram
        """

        audio_path = self.filepath # The path of the .wav file
        sample_rate = librosa.get_samplerate(self.filepath) # Get current .wav file's sample rate
        x, sr = librosa.load(audio_path, sr=sample_rate) # Load the .wav file
        X = librosa.stft(x) # Use the Short-time Fourier transformation (stft) from librosa
        Xdb = librosa.amplitude_to_db(abs(X)) # Convert an amplitude spectrogram to dB-scaled spectrogram
        
        # Create the spectrogram figure with time as the x-axis and log as the y-axis 
        plt.figure(figsize=(14, 5))
        librosa.display.specshow(Xdb, sr=sample_rate, x_axis='time', y_axis='linear')
        plt.colorbar()

        # Save the figure to the right folder
        name_ = os.path.basename(audio_path).split(".")[0] + '.png'
        spec_path = os.path.join('./static/images/', date.today().strftime(r'%m%d%Y'))
        if os.path.exists(spec_path) == False:
            os.makedirs(spec_path)
        spec_img_path = os.path.join(os.path.join('./static/images/', date.today().strftime(r'%m%d%Y'), name_))
        plt.savefig(spec_img_path, facecolor='w')
        plt.clf()

        # Return the path of the spectrogram
        return spec_img_path[1:]