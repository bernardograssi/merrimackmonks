# Import all necessary libraries
import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class EmailSender:
    """
    A class used to send an email to the user when a password reset is requested.

    Attributes
    ----------
    sender_email : str
        The email address of the sender, which in this case is the merrimackmonks gmail account.
    receiver_email : str
        The email address of the receiver of the email, which is inputted by the user.
    password : str
        The app password of the gmail API used to send the email.
    token : str
        The token used to perform the password reset.
    message : dict
        The message object is a dictionary that stores the subject, from, and to elements of the email.
    text : str
        The body of the email.

    Methods
    -------
    sendEmail()
        This method sends the email to the user using smtp.gmail.com. 

    """
    def __init__(self, receiver_email, token):
        """
        Parameters
        ----------
        receiver_email : str
            The email address of the receiver of the email, which is inputted by the user.
        token : str
            The token used to perform the password reset.
        """

        self.sender_email = "merrimackmonks@gmail.com"
        self.receiver_email = receiver_email
        self.password = "cutvohwzznbcmiuq"
        self.token = token
        self.message = MIMEMultipart("alternative")
        self.message["Subject"] = "Password Reset Request [Squirrel Calls Application]"
        self.message["From"] = self.sender_email
        self.message["To"] = self.receiver_email
        self.text = """\
        Hello,
        This is the token requested from the Squirrel Call App for your password reset: 
        
        %s 
            
        The token is valid for 24 hours.
            
        Thank you,
        Merrimack Monks Support Team""" % self.token
        self.part1 = MIMEText(self.text, "plain")
        self.message.attach(self.part1)

    def sendEmail(self):
        """
        This method sends the email to the user using smtp.gmail.com.
        """

        # Create default context.
        context = ssl.create_default_context()

        # Send email with smtp.gmail.com.
        with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as server:
            server.login(self.sender_email, self.password)
            server.sendmail(
                self.sender_email, self.receiver_email, self.message.as_string()
            )
