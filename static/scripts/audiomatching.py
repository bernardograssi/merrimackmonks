from scipy.signal import find_peaks
from static.scripts.spectrogram import Spectrogram
import librosa
import time
import re
import os
import numpy as np


class PeakMatch:
    """
    A class used to match the files submitted by the user with the fingerprints saved.

    Attributes
    ----------
    files: list
        The files representing the fingerprints saved.
    dir: str
        The path of the program's current directory.
    peakArray: list
        The peaks found in each of the fingerpints.
    test_result: list
        The result of the matches as a dictionary.

    Methods
    -------
    __singleCreate():
        This method populates the peakArray list with the peaks found in each of the fingerprints saved.

    match():
            This method performs the audio-matching algorithm for each of the files received as the parameter 
            and returns the tes_result dictionary with the results of the matching process.

    """

    def __init__(self):
        """
        Class constructor
        """
        self.files, self.strings = [], [] # Files and strings
        self.dir = os.path.join(os.path.dirname(__file__), "fingerprints") # Current directory

        # For each fingerprint file found in the fingerprints directory, save the data in the files array.
        for fingerprint in os.listdir(self.dir):
            with open(os.path.join(self.dir, fingerprint), 'r') as f:
                data = f.readline()
                f.close()
                self.files.append(data)
            self.strings.append('')

        self.peakArray = [None for x in range(len(self.files))] # Initialize peakArray to None in every single entry.
        self.test_result = {} # Initialize empty test_result dictionary.
        self.__singleCreate() # Populate the peakArray with the data from the fingerprints.

    def __singleCreate(self):
        """
        This method populates the peakArray list with the peaks found in each of the fingerprints saved.
        """

        # For each item in the file, populate the peakArray and the string
        counter = 0
        for item in self.files:
            text = item
            tempArr = text.split(',')
            tempArr = tempArr[:-1]
            t1 = "".join("<"+str(i)+">" for i in tempArr)
            string1 = re.compile(t1)
            self.peakArray[counter] = string1 
            counter += 1
            
    def match(self, audio):
        """
        Parameters
        ----------
        audio: list
            A list of audio files from the user input.

        This method performs the audio-matching algorithm for each of the files received as the parameter 
        and returns the tes_result dictionary with the results of the matching process.
        """
        x, sr = librosa.load(audio)
        result = False
        peaks, _ = find_peaks(x, height=0)
        temp = x[peaks]
        newPeak = "".join("<"+str(i)+">" for i in temp)

        for peak in self.peakArray:
            search = re.search(peak, newPeak)
            if search:
                # Find timestamp of match
                duration = librosa.get_duration(filename=audio)
                try:
                    found = np.array(list(map(float, search.group()[1:-1].split("><"))), dtype=np.float32)
                except:
                    return False, 0
                ind = np.isin(temp, found).nonzero()[0]
                matchTime = round((duration * peaks[ind][0]) / len(x), 2)
                return True, matchTime

        return result, 0

    def matchFiles(self, audioFiles: list, zipcode: str):
        """
        Parameters
        ----------
        audioFiles: list
            The list of audio files from user's input.
        zipcode: str
            The zip code the user has input in the file submission page.

        Processes the audio-matching algorithm and creates a list of dictionaries containing the results for each file submitted by the user, which is returned to the caller
        """

        # Simulate audio-matching algorithm for each file submitted
        for audioFile in audioFiles:

            isMatch = False
            isMatch, matchTime = self.match(audioFile)
            curr_spec = ''

            # If there is a match, generate spectrogram
            if isMatch:
                # Create Spectrogram object and generate a spectrogram image from the current .wav file
                spec = Spectrogram(filepath=audioFile)
                curr_spec = spec.generateSpectrogram()

            # Create the dictionary for the current file, containing the name of the file,
            # whether it is a match or not, the ZIP code, the date, and the spectrogram path
            self.test_result[audioFile] = {
                "filename": audioFile,
                "ismatch": isMatch,
                "matchTime": matchTime,
                "zipcode": zipcode,
                "thedate": time.strftime("%m/%d/%Y"),
                "spec": curr_spec
            }

        # Return the list of dictionaries to the caller
        return self.test_result