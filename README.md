# Merrimack Monks Capstone Project Fall 21' #
### What is this repository for? ###
* This repository contains the whole project developed by Bernardo Santos and Liam Twomey for Capstone class in the Fall of 2021, under the guidance of Prof. Christopher Stuetzle. The project aims at creating a website that allows users to drop .WAV files containing recordings from squirrel calls in the woods and receive a feedback about the recordings, stating if it is either a match or not. 

### How do I get set up? ###

* Initially, in the development stage, please clone this repository to VS Code through the BitBucket and VS Code functionalities. Once the repository is cloned and the folder is open in VS Code, please open a terminal in the directory and run the following commands:

> C:\Users\username\project\path> set FLASK_APP=main.py  
> C:\Users\username\project\path> set FLASK_ENV=development  
> C:\Users\username\project\path> flask run  


### Developers Information ###

* Bernardo Santos -> santosbe@merrimack.edu
* Liam Twomey -> twomeyl@merrimack.edu